<?php
	/**
	* Клиент SOAP для тестирования сервера
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\SOAPClient
	*/

	namespace Application ;

	class SoapClient extends \SoapClient {
		/**
		* конструктор
		* @param $wsdl string - путь к wsdl
		* @param $options array - настройки
		*/
		function __construct( $wsdl , $options = array( ) ) {
			parent::__construct( $wsdl , $options ) ;

			/**
			* создание объекта сервера SoapServer
			*/
			$this->server = new \SoapServer( $wsdl , $options ) ;
		}

		/**
		* Выполнение запроса
		* @param $request mixed - запрос
		* @param $location string - адрес
		* @param $action string - действие
		* @param $version string - версия
		* @return string - ответ
		*/
		function __doRequest( $request , $location , $action , $version ) {
			ob_start( ) ;
			$this->server->handle( $request ) ;
			$response = ob_get_contents( ) ;
			ob_end_clean( ) ;

			return $response ;
		}
	}