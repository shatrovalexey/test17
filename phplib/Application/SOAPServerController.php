<?php
	/**
	* Контроллер сервера SOAP
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\SOAPServerController
	*/

	namespace Application ;

	class SOAPServerController extends Application {
		/**
		* Подсчёт перестановок символов без повторов в строках
		* @param $params array of string - список строк для анализа
		* @return array of struct { int , float } - массив массивов из количества перестановок и времени на вычисление для каждой строки
		*/
		function combinations( $params ) {
			/**
			* @var $results array - результаты выполнения
			* @var $combinations array - данные по количеству перестановок из настроек
			*/
			$results = array( ) ;
			$combinations = &$this->config->combination ;

			/**
			* просмотр входных строк
			* @var $param string - очередная строка
			*/
			foreach ( $params as &$param ) {
				/**
				* начало замера времени выполнения
				*/
				$this->__timer_start( ) ;

				/**
				* Если длина очередной строки не входит в обусловленный задачей диапазон,
				* то записываю количество перестановок null.
				*/
				if ( mb_strlen( $param ) >= count( $combinations ) ) {
					$results[] = array( null , $this->__timer_finish( ) ) ;

					continue ;
				}

				/**
				* подсчёт количества уникальных символов
				*/
				$count_chars_idx = $this->__mb_count_chars( $param ) ;

				/**
				* Если длина очередной строки пустая или её значение null, то количество перестановок 1.
				*/
				if ( empty( $count_chars_idx ) ) {
					$results[] = array( 1 , $this->__timer_finish( ) ) ;

					continue ;
				}

				/**
				* Записываю количество перестановок из настроек
				*/
				$results[] = array( $combinations[ $count_chars_idx - 1 ] , $this->__timer_finish( ) ) ;
			}

			return $results ;
		}

		/**
		* Подсчёт количества уникальных символов в строке
		* @param $param string - строка с символами
		* @return integer - количество никальных имволов в строке
		*/
		protected function __mb_count_chars( &$param ) {
			/**
			* Если $param содержит null, то результат тоже null
			*/
			if ( is_null( $param ) ) {
				return null ;
			}

			/**
			* @var $len int - количество символов мультибайтовой строки
			* @var $result array - список уникальных символов строки
			*/
			$len = mb_strlen( $param ) ;
			$result = array( ) ;

			/**
			* просмотр символов строки с конца
			*/
			while ( $len -- > 0 ) {
				/**
				* очередной символ
				*/
				$char = mb_substr( $param , $len , 1 ) ;

				/**
				* следующий символ, если символ уже сохранён в $result
				*/
				if ( in_array( $char , $result ) ) {
					continue ;
				}

				/**
				* запомнить символ
				*/
				$result[] = $char ;
			}

			/**
			* подсчёт количества элементов в $result
			*/
			return count( $result ) ;
		}
	}