<?php
	/**
	* Общий класс с общими методами для прочих классов
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\Application
	*/

	namespace Application ;

	class Application extends \stdClass {
		/**
		* @const CONFIG_FILE_NAME string - путь к файлу настроек по-умолчанию
		*/
		const CONFIG_FILE_NAME = 'config/config.json' ;

		/**
		* @var $config array - структура данных настроек
		*/
		var $config ;

		/**
		* @var $creator stdClass - объект-создатель
		*/
		var $creator ;

		/**
		* @var $start_time float - время начала замера скорости выполнения кода
		*/
		protected $start_time ;

		/**
		* Конструктор
		*/
		function __construct( $creator = null ) {
			if ( empty( $creator ) ) {
				/**
				* Загрузка файла настроек
				*/
				$this->configure( ) ;
				$this->__prepare( ) ;
			} else {
				/**
				* Подключение настроек создателя
				*/
				$this->creator = &$creator ;
				$this->config = &$this->creator->config ;
			}
		}

		/**
		* Возврат свойства $this->$key или заполнение $sub( ), если оно пусто
		* @param $key string - имя свойства
		* @param $sub function - функция для заполнения свойства $key
		* @return mixed - значение свойства $this->$key
		*/
		protected function __coalesce( $key , $sub ) {
			if ( ! empty( $this->$key ) ) {
				return $this->$key ;
			}

			$this->$key = $sub( ) ;

			return $this->$key ;
		}

		/**
		* Подготовка среды
		* @return $this
		*/
		protected function __prepare( ) {
			foreach ( $this->config->ini_set as $key => $value ) {
				ini_set( $key , $value ) ;
			}

			return $this ;
		}

		/**
		* Загрузка настроек
		* @param $file_name string - имя файла настроек
		* @return mixed - загруженные данные настроек
		*/
		protected function __configure( $file_name = self::CONFIG_FILE_NAME ) {
			/**
			* @var $data string - строка с настройками из файла $file_name
			*/

			$file_name = __DIR__ . '/../../' . $file_name ;

			$data = file_get_contents( $file_name ) ;
			if ( empty( $data ) ) {
				exit( 0 ) ;
			}

			/**
			* @var $result mixed - структура данных с настройками
			*/
			$result = json_decode( $data ) ;
			if ( empty( $result ) ) {
				exit( 0 ) ;
			}

			return $result ;
		}

		/**
		* Загрузка настроек в $this
		* @param $file_name string - имя файла настроек
		* @return mixed - загруженные данные настроек
		*/
		function configure( $file_name = self::CONFIG_FILE_NAME ) {
			$result = $this->__coalesce( 'config' , function( ) use( &$file_name ) {
				return $this->__configure( $file_name ) ;
			} ) ;

			return $result ;
		}

		/**
		* Генерация текущего выремени внутри процесса
		* @return float
		*/
		protected function __timer( ) {
			return microtime( true ) ;
		}

		/**
		* "Запуск" таймера для замера времени выполнения кода
		* @return float
		*/
		protected function __timer_start( ) {
			$this->start_time = $this->__timer( ) ;
		}

		/**
		* Получение времени выполнения кода
		* @return float
		*/
		protected function __timer_finish( ) {
			return $this->__timer( ) - $this->start_time ;
		}
	}