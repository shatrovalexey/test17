<?php
	/**
	* Класс для запуска сервера SOAP и клиента
	*
	* @author Shatrov Aleksej <mail@ashatrov.ru>
	* @version 1.0
	* @package Application\SOAP
	*/

	namespace Application ;

	class SOAP extends Application {
		/**
		* @var $client SOAPClient - SOAP-клиент
		*/
		var $client ;

		/**
		* Запуск теста
		* @param $file_name string - путь к файлу со списком строк для теста
		* @return array of struct{ int , float } - результат выполнения SOAP-метода
		*/
		function consume( $file_name = null ) {
			/**
			* Если имя файла со строками пустое, то использовать имя файла из настроек
			*/
			if ( empty( $file_name ) ) {
				$file_name = $this->config->data_file ;
			}

			/**
			* Чтение файла со строками для теста в массив
			*/
			$lines = file( __DIR__ . '/../../' . $file_name , \FILE_IGNORE_NEW_LINES ) ;

			/**
			* @var $soap_controller Application\SOAPServerController - объект контроллера сервера SOAP
			* @var $this->client Application\SOAPClient - объект клиента SOAP
			*/
			$soap_controller = new SOAPServerController( $this ) ;
			$this->client = new SOAPClient( $this->config->wsdl ) ;

			$this->client->server->setObject( $soap_controller ) ;

			/**
			* запуск тестового метода и возвращение результатов
			*/
			return $this->client->combinations( $lines ) ;
		}
	}